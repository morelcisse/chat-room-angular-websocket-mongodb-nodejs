import { environment } from "src/environments/environment";

export const makeDoubleDigit = (x: number): string | number => {
	return x < 10 ? "0" + x : x;
};

export const formateTime = (value: number | string | Date) => {
	const date = value ? new Date(value) : new Date();

	const year = date.getFullYear();
	const month = date.getMonth() + 1;
	const day = date.getDate();

	const hour = date.getHours();
	const minute = date.getMinutes();
	const second = date.getSeconds();

	return (
		makeDoubleDigit(day) +
		"-" +
		makeDoubleDigit(month) +
		"-" +
		makeDoubleDigit(year) +
		" " +
		makeDoubleDigit(hour) +
		":" +
		makeDoubleDigit(minute) +
		":" +
		makeDoubleDigit(second)
	);
};

/**
 * Please sleep !
 */
export const sleep = (ms: number = 1000) => {
	return new Promise((resolve) => setTimeout(resolve, ms));
};

/* -------------------------------------------------------------------------- */
/*                                   STORAGE                                  */
/* -------------------------------------------------------------------------- */

export const setToStorage = (key: string, value: string): string => {
	window.localStorage.setItem(key, value);
	window.sessionStorage.setItem(key, value);

	return value;
};

export const getFromStorage = (key: string): string => {
	return (window.localStorage.getItem(key) || window.sessionStorage.getItem(key)) as string;
};

export const deleteFromStorage = (key: string) => {
	window.localStorage.removeItem(key);
	window.sessionStorage.removeItem(key);
};

/* -------------------------------------------------------------------------- */

/**
 * @param form
 * @param fieldName
 */
export const checkInput = (form: any, fieldName: string): any => {
	const input = form[fieldName].errors;

	if (input) {
		if (input.required || input.min) {
			return "Ce champ est requis.";
		} else if (input.max) {
			return "Ce champ à une limite de 1000 caractères maximum.";
		} else if (input.email) {
			return "Veuillez saisir une adresse mail valide.";
		} else if (input.pattern) {
			return "Vous essayez de rentrer des caractères non autorisés.";
		}
	}
};

/**
 * Scroll into view
 */
export const scrollIntoView = (el: Element | string) => {
	const element = typeof el === "string" ? document.querySelector(el) : el;

	if (!element) return;

	setTimeout(() => {
		element.scrollTop = element.scrollHeight - element.clientHeight;
	}, 100);
};

export const isDev = () => environment.NODE_ENV === "development";
export const isProd = () => environment.NODE_ENV === "production";

/**
 * Get and/or set uuid variable in storage
 */
export const uuid = (): string => getFromStorage("uuid") || setToStorage("uuid", createUUID());

/**
 * Generating random whole numbers in a specific range
 *
 * @param {Number} min
 * @param {Number} max
 */
export const randomNumber = (min: number = 0, max: number = 4) => {
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const monthNames = [
	"Janvier" /* "January" */,
	"Février" /* "February" */,
	"Mars" /* "March" */,
	"Avril" /* "April" */,
	"Mai" /* "May" */,
	"Juin" /* "June" */,
	"Juillet" /* "July" */,
	"Août" /* "August" */,
	"Septembre" /* "September" */,
	"Octobre" /* "October" */,
	"Novembre" /* "November" */,
	"Décembre" /* "December" */,
];

/**
 * Mettre à jour les valeurs du storage chaque 1 secondes
 */
export const storageKeys: string[] = ["connected", "token", "session_status", "participants", "uuid"];

/**
 * Generate UUID
 */
export const createUUID = (): string => {
	let dt: number = new Date().getTime();
	const uuid: string = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) => {
		// tslint:disable-next-line: no-bitwise
		const r = (dt + Math.random() * 16) % 16 | 0;
		dt = Math.floor(dt / 16);

		// tslint:disable-next-line: no-bitwise
		return (c === "x" ? r : r & 0x3 || 0x8).toString(16);
	});

	return uuid;
};
