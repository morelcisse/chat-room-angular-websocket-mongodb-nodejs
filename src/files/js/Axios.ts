import axios, { AxiosInstance, AxiosRequestConfig } from "axios";

import { environment } from "src/environments/environment";

const axiosInstance = (config: AxiosRequestConfig = {}): AxiosInstance => {
	const token: string = window.localStorage.getItem("token");
	const defaultConfig: AxiosRequestConfig = {
		...config,
		baseURL: environment.API_URL,
		headers: { Authorization: `Bearer ${token}` },
	};

	if (!token) delete defaultConfig["headers"];
	return axios.create(defaultConfig);
};

export default axiosInstance;
