import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Subscription, from, lastValueFrom } from "rxjs";
import ObjectID from "bson-objectid";
import { HttpClient } from "@angular/common/http";

import { IMessage, IUser } from "src/interfaces";
import { SharedService } from "./../services/shared.service";
import { MessagesService } from "./services/messages.service";
import { UsersService } from "./../services/users.service";
import { environment } from "src/environments/environment";
import { scrollIntoView, setToStorage, getFromStorage, formateTime } from "src/files/js/Helpers";
import { AuthService } from "src/app/auth/services/auth.service";
import { io, Socket } from "socket.io-client";
import { ToastrService } from "ngx-toastr";

@Component({
	selector: "app-chat",
	templateUrl: "./chat.component.html",
	styleUrls: ["./chat.component.scss"],
})
export class ChatComponent implements OnInit, OnDestroy {
	@ViewChild("chatSendBtn") chatSendBtn!: ElementRef;
	@ViewChild("chatInput") chatInput!: ElementRef;

	private socket: Socket;
	public chatFormGroup!: FormGroup;

	private messagesServices: { [x: string]: Subscription } = {};
	private usersServices: { [x: string]: Subscription } = {};
	public submittedChat = false;
	public loaderMessages = true;
	public participants: string[] = [];
	public channelName = "custom_channel_name";

	constructor(
		public messageService: MessagesService,
		public userService: UsersService,
		public authService: AuthService,
		public sharedService: SharedService,
		private formBuilder: FormBuilder,
		private httpClient: HttpClient,
		private toastr: ToastrService
	) {
		this.socket = io(environment.API_URL); // Connect Socket with server URL
	}

	private async initApp() {
		setTimeout(async () => {
			let token = getFromStorage("token");

			if (!token) return this.authService.clearSession();

			token = getFromStorage("token");

			const response = await lastValueFrom(
				this.httpClient.post<{ message: string; user: IUser }>(`${environment.API_URL}/verify`, {
					token,
				})
			);
			const message = response.message;

			if (message === "User not exist" || message === "User not connected") {
				return this.authService.clearSession();
			}

			const user: IUser = response.user;
			this.userService.user = { ...user, token };

			this.handleConnected();
		}, 300);
	}

	/**
	 * New message notification
	 */
	private initNewMessageSubscription() {
		this.messagesServices["onNewMessage"] = this.messageService
			.onNewMessage(this.channelName)
			.subscribe((message: IMessage) => {
				const sender: IUser = message.user;
				const currentMessages = this.messageService.messages ?? [];
				this.messageService.messages = [...currentMessages, message];

				if (sender._id !== this.userService.user._id) {
					this.toastr.success(`Nouveau message de ${sender.name} !`);
				}
				setTimeout(() => scrollIntoView(".main-chat"), 300);
			});
	}

	/**
	 * User join or leave room
	 */
	private initJoinLeaveRoomSubscription(): void {
		const onRoomCb = (data: any, msg1: string, msg2: string) => {
			const user: IUser = data.user;

			if (!this.participants.includes(this.userService.user._id)) return;

			if (user._id === this.userService.user._id) {
				this.toastr.info(msg1);

				this.userService.user = { ...user };
			}

			if (user._id !== this.userService.user._id) {
				this.toastr.info(msg2);
			}
		};

		this.usersServices["onJoinRoom"] = this.userService.onJoinRoom().subscribe((data) => {
			if (!this.userService.connected()) return;

			const user: IUser = data.user;
			const name: string = user.name;

			onRoomCb(data, "Vous êtes connecté au chat.", `${name} est connecté au chat.`);
		});

		this.usersServices["onLeaveRoom"] = this.userService.onLeaveRoom().subscribe((data) => {
			if (!this.userService.connected()) return;

			const user: IUser = data.user;
			const name: string = user.name;

			onRoomCb(data, "Vous êtes déconnecté au chat.", `${name} est déconnecté au chat.`);
		});
	}

	ngOnInit(): void {
		this.chatFormGroup = this.formBuilder.group({
			message: new FormControl({ value: "", disabled: true }, [
				Validators.required,
				Validators.pattern(/^[a-zA-Z0-9\u00C0-\u00FF _.:,;!?#*%+( )\]\[&{}'"@-]*$/), // https://www.regextester.com/106533
				Validators.min(1),
				Validators.max(1000),
			]),
		});

		this.initApp();
	}

	ngOnDestroy(): void {
		this.messagesServices["onNewMessage"]?.unsubscribe();
		this.usersServices["onNewUser"]?.unsubscribe();
		this.usersServices["onJoinRoom"]?.unsubscribe();
		this.usersServices["onLeaveRoom"]?.unsubscribe();
	}

	public disabledChatInput(enable: boolean) {
		const formGroupMessage = this.chatFormGroup.get("message") as AbstractControl<any, any>;
		const input: HTMLElement = this.chatInput.nativeElement;

		if (enable) {
			// Add disabled attr on element
			formGroupMessage.setValue("");
			formGroupMessage.disable();
			this.chatSendBtn.nativeElement.setAttribute("disabled", "true");
			input.style.pointerEvents = "none";
		} else {
			this.chatSendBtn.nativeElement.removeAttribute("disabled");
			formGroupMessage.enable();
			input.style.pointerEvents = "initial";
		}
	}

	public disabledChatInputEmitter(value: boolean) {
		this.disabledChatInput(value);
	}

	get c() {
		return this.chatFormGroup.controls;
	}

	private onReset() {
		this.submittedChat = false;

		this.chatFormGroup.reset();
	}

	public getDate(messageId: string): string {
		const timestamp = new ObjectID(messageId).getTimestamp();

		return formateTime(timestamp);
	}

	private setParticipants(userId: string) {
		const participants: string[] = this.participants || JSON.parse(getFromStorage("participants"));

		if (this.messageService.messages.length) {
			this.messageService.messages.map(({ user }) => {
				if (!participants.includes(user._id)) participants.push(user._id);
			});
		} else {
			// Add current connected user in the list of participants
			participants.push(this.userService.user._id);
		}

		this.participants = participants;

		setToStorage("participants", JSON.stringify(participants)); // Define participants
	}

	/**
	 * Get messages
	 */
	public async getMessages(userId: string) {
		const response = await lastValueFrom(this.messageService.getMessages());
		this.messageService.messages = response || [];

		this.setParticipants(userId);

		setTimeout(() => {
			scrollIntoView(".main-chat");
			setTimeout(() => this.sharedService.scrollTo("message"), 500);
		}, 600);

		this.loaderMessages = false;
	}

	/**
	 * Send message
	 */
	public async newMessage(message: string) {
		let data = { message, channelName: this.channelName } as any;
		data["user"] = this.userService.user;
		// Clear user for message obj
		delete data["user"].token;
		delete data["user"].connected;
		delete data["user"].uuid;

		const response = await lastValueFrom(
			this.httpClient.post<IMessage>(`${environment.API_URL}/create/message`, data)
		);
		this.submittedChat = false;

		this.socket.emit("new_message", { message: response.message, channelName: this.channelName });
		this.sharedService.scrollTo("message");
		this.onReset();
	}

	/**
	 * Submit new message
	 */
	public onSubmitChat() {
		this.submittedChat = true;

		if (this.chatFormGroup.invalid) return;
		if (!this.userService.connected()) return this.disabledChatInput(true);

		const message: string = this.c["message"].value;

		this.newMessage(message);
	}

	/**
	 * Join room
	 */
	public joinRoom(user: IUser) {
		this.socket.emit("join_room", { channelName: this.channelName, user });
	}

	public async handleConnectedEmitter(token: string) {
		await this.handleConnected();
	}

	public async handleConnected() {
		const currentUser = this.userService.user;
		const userId: string = currentUser._id;

		await this.getMessages(userId); // Get messages
		this.joinRoom(currentUser); // Current user join his channel directly
		this.initNewMessageSubscription();
		this.disabledChatInput(false);

		setToStorage("connected", userId);
		setToStorage("session_status", "active");
		this.initJoinLeaveRoomSubscription();
	}

	public logout = () => {
		from(this.authService.logout()).subscribe((message: string) => {
			this.sharedService.loginAction = false;
			this.sharedService.registrationAction = false;

			this.authService.clearSession();
			this.chatFormGroup.reset();
			// window.location.reload();
			this.toastr.success("Vous êtes déconnecté(e).");
		});
	};
}
