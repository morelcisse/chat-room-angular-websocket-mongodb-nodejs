import { Injectable } from "@angular/core";
import { io, Socket } from "socket.io-client";
import { Observable, BehaviorSubject } from "rxjs";
import { HttpClient } from "@angular/common/http";

import { environment } from "src/environments/environment";
import { IMessage } from "src/interfaces";

@Injectable({
	providedIn: "root",
})
export class MessagesService {
	private socket: Socket;

	public messages$: BehaviorSubject<IMessage[]> = new BehaviorSubject<IMessage[]>([]);

	constructor(public httpClient: HttpClient) {
		this.socket = io(environment.API_URL); // Connect Socket with server URL
	}

	set messages(messages: IMessage[]) {
		this.messages$.next(messages);
	}

	get messages() {
		return this.messages$.getValue();
	}

	/**
	 * Listen new message
	 */
	public onNewMessage(channelName: string): Observable<IMessage> {
		return new Observable<IMessage>((observer) => {
			this.socket.on(channelName, (data) => observer.next(data.message));
		});
	}

	/**
	 * Get all messages
	 */
	public getMessages(): Observable<IMessage[]> {
		return this.httpClient.get<IMessage[]>(`${environment.API_URL}/messages`);
	}
}
