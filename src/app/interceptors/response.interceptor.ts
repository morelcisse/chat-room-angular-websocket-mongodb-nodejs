import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { tap, catchError } from "rxjs/operators";

import { UsersService } from "src/app/services/users.service";
import { getFromStorage, setToStorage } from "src/files/js/Helpers";
import { AuthService } from "src/app/auth/services/auth.service";
import { ToastrService } from "ngx-toastr";

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
	constructor(
		private userService: UsersService,
		private authService: AuthService,
		private toastr: ToastrService
	) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		if (this.userService.connected()) {
			const token = this.userService.user.token ?? getFromStorage("token");
			req = req.clone({
				setHeaders: {
					Authorization: `Bearer ${token}`,
				},
			});
		}

		return next.handle(req).pipe(
			tap((evt) => {
				if (evt instanceof HttpResponse) {
					if (evt.body && evt.body.message === "Token expired") {
						setToStorage("session_status", "expired");
						this.toastr.error("Votre session à expirée. Vous allez être déconnecté(e)...");
						this.authService.clearSession();
					} else if (evt.body && evt.body.message === "Token invalid") {
						this.toastr.error("Une erreur s'est produite, veuillez vous reconnecter.");
						this.authService.clearSession();
						return true;
					}
				}
			}),
			catchError((error) => {
				this.toastr.error("Une erreur s'est produite, veuillez vous reconnecter.");
				this.authService.clearSession();
				return of(error);
			})
		);
	}
}
