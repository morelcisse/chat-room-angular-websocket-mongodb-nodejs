import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ChatComponent } from "src/app/chat/chat.component";

const routes: Routes = [{ path: "", component: ChatComponent, pathMatch: "full" }];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
export { routes };
