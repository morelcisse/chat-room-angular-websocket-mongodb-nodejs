import { NgModule, Provider } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxScrollTopModule } from "ngx-scrolltop";

import { AuthComponent } from "src/app/auth/auth.component";
import { ChatComponent } from "src/app/chat/chat.component";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ResponseInterceptor } from "src/app/interceptors/response.interceptor";
import { ToastrModule } from "ngx-toastr";

const declarations = [AppComponent, ChatComponent, AuthComponent];

const imports = [
	BrowserModule,
	AppRoutingModule,
	ReactiveFormsModule,
	FormsModule,
	NgxScrollTopModule,
	BrowserAnimationsModule,
	HttpClientModule,
	ToastrModule.forRoot(),
];

const providers: Provider[] = [{ provide: HTTP_INTERCEPTORS, useClass: ResponseInterceptor, multi: true }];

@NgModule({
	declarations,
	imports,
	providers,
	bootstrap: [AppComponent],
})
export class AppModule {}
export { declarations, imports, providers };
