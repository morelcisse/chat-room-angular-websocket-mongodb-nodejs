import { Injectable } from "@angular/core";
import { io, Socket } from "socket.io-client";
import { Observable, BehaviorSubject } from "rxjs";
import { HttpClient } from "@angular/common/http";

import { environment } from "../../environments/environment";
import { IUser } from "../../interfaces";
import { getFromStorage } from "src/files/js/Helpers";

@Injectable({
	providedIn: "root",
})
export class UsersService {
	private socket: Socket;

	public user$: BehaviorSubject<IUser> = new BehaviorSubject({} as IUser);

	constructor(public httpClient: HttpClient) {
		this.socket = io(environment.API_URL); // Connect Socket with server URL
	}

	set user(user: IUser) {
		this.user$.next(user);
	}

	get user() {
		return this.user$.getValue();
	}

	public connected(): boolean {
		const token: string = getFromStorage("token");

		return token !== null;
	}

	public onNewUser(): Observable<{ user: IUser; channelName: string }> {
		return new Observable<{ user: IUser; channelName: string }>((observer) => {
			this.socket.on("new_registration_notify", (data: { user: IUser; channelName: string }) => {
				return observer.next(data);
			});
		});
	}

	public onJoinRoom(): Observable<{ user: IUser; channelName: string }> {
		return new Observable<{ user: IUser; channelName: string }>((observer) => {
			this.socket.on("join_room_notify", (data: { user: IUser; channelName: string }) => {
				return observer.next(data);
			});
		});
	}

	public onLeaveRoom(): Observable<{ user: IUser; channelName: string }> {
		return new Observable<{ user: IUser; channelName: string }>((observer) => {
			this.socket.on("leave_room_notify", (data: { user: IUser; channelName: string }) => {
				return observer.next(data);
			});
		});
	}
}
