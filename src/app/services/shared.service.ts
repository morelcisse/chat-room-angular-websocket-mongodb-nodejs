import { Injectable } from "@angular/core";
import { NgAnimateScrollService } from "ng-animate-scroll";
import { HttpClient } from "@angular/common/http";

import { checkInput } from "src/files/js/Helpers";
import { UsersService } from "src/app/services/users.service";

@Injectable({
	providedIn: "root",
})
export class SharedService {
	public registrationAction = false;
	public loginAction = false;

	constructor(
		private animateScrollService: NgAnimateScrollService,
		public userService: UsersService,
		public httpClient: HttpClient
	) {}

	public scrollTo(elemenID: string = "message", duration: number = 750) {
		const el = document.getElementById(elemenID);

		if (!el) return;

		return this.animateScrollService.scrollToElement(elemenID, duration);
	}

	public checkInput = (fieldName: string, form: any): string => checkInput(form, fieldName);
}
