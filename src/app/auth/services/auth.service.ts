import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Socket, io } from "socket.io-client";

import { UsersService } from "src/app/services/users.service";
import { environment } from "src/environments/environment";
import { deleteFromStorage } from "src/files/js/Helpers";
import { SharedService } from "src/app/services/shared.service";

@Injectable({
	providedIn: "root",
})
export class AuthService {
	private socket: Socket;

	constructor(
		public userService: UsersService,
		public httpClient: HttpClient,
		public sharedService: SharedService
	) {
		this.socket = io(environment.API_URL); // Connect Socket with server URL
	}

	public async logout(): Promise<string> {
		return this.httpClient
			.post(`${environment.API_URL}/logout`, {
				userId: this.userService.user._id,
			})
			.toPromise()
			.then((response: any) => response.message);
	}

	/**
	 * @param {Function} cb
	 */
	public clearSession(cb?: () => void) {
		const user = this.userService.user;

		this.socket.emit("leave_room", {
			channelName: user._id,
			user: { ...user, connected: false },
		});

		deleteFromStorage("connected");
		deleteFromStorage("token");
		deleteFromStorage("session_status");
		deleteFromStorage("participants");
		setTimeout(() => this.sharedService.scrollTo("chat"), 300);

		if (cb) cb();
	}
}
