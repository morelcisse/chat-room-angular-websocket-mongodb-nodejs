import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { io, Socket } from "socket.io-client";
import { HttpClient } from "@angular/common/http";
import omit from "lodash/omit";

import { UsersService } from "src/app/services/users.service";
import { SharedService } from "src/app/services/shared.service";
import { sleep, uuid, setToStorage } from "src/files/js/Helpers";
import { environment } from "src/environments/environment";
import { IUser } from "src/interfaces";
import { ToastrService } from "ngx-toastr";

@Component({
	selector: "app-auth",
	templateUrl: "./auth.component.html",
	styleUrls: ["./auth.component.scss"],
})
export class AuthComponent implements OnInit {
	@Output() handleConnected = new EventEmitter();

	private socket: Socket;
	public registrationLoginFormGroup!: FormGroup;

	public submittedStartRegistrationLogin = false;
	public loaderRegistrationLogin = false;

	constructor(
		public userService: UsersService,
		public sharedService: SharedService,
		private formBuilder: FormBuilder,
		private httpClient: HttpClient,
		private toastr: ToastrService
	) {
		this.socket = io(environment.API_URL); // Connect Socket with server URL
	}

	ngOnInit(): void {
		this.setRegistrationLoginFormGroup("registration");
	}

	public handleRegistrationAction() {
		this.sharedService.registrationAction = true;
		this.sharedService.loginAction = false;

		this.setRegistrationLoginFormGroup("registration");
		this.sharedService.scrollTo("chat");
	}

	public handleLoginAction() {
		this.sharedService.loginAction = true;
		this.sharedService.registrationAction = false;

		this.setRegistrationLoginFormGroup("login");
		this.sharedService.scrollTo("chat");
	}

	public setRegistrationLoginFormGroup(action: "login" | "registration") {
		let controlsConfig = {
			name: ["", [Validators.required]],
			email: ["", [Validators.required, Validators.email]],
		} as object;

		if (action === "login") {
			controlsConfig = omit(controlsConfig, "name");
		}

		this.registrationLoginFormGroup = this.formBuilder.group(controlsConfig);

		this.registrationLoginFormGroup.valueChanges.subscribe((values) => {
			const button = document.getElementById("action-btn");

			if (button) {
				if (this.registrationLoginFormGroup.status === "INVALID") {
					button.style.display = "none";
					return;
				}

				if (action === "login") {
					button.style.display = "flex";
				} else {
					button.style.display = values.name.length > 1 ? "flex" : "none";
				}
			}
		});
	}

	public onSubmitRegistrationLogin() {
		this.submittedStartRegistrationLogin = true;

		if (this.registrationLoginFormGroup.invalid) return;

		this.loaderRegistrationLogin = true;

		if (this.sharedService.loginAction) return this.login();

		return this.registration();
	}

	get r() {
		return this.registrationLoginFormGroup.controls;
	}

	public async registration() {
		const email: string = this.r["email"].value;
		const name: string = this.r["name"].value;

		await sleep();

		this.httpClient
			.post(`${environment.API_URL}/registration`, {
				email,
				name,
				uuid: uuid(),
			})
			.subscribe((response: any) => {
				const message: string = response.message;

				if (message === "User already exist") {
					this.toastr.warning("Cet utilisateur existe déjà. Veuillez vous connectez directement.");
				} else if (message === "User created") {
					const token: string = response.token;
					const user: IUser = response.user;
					const channelName: string = user._id;
					this.userService.user = { ...user, token };

					setToStorage("token", token);
					this.socket.emit("new_registration", { channelName, user: this.userService.user });
					this.registrationLoginFormGroup.reset();
					this.handleConnected.emit();
				}

				this.loaderRegistrationLogin = false;
			});
	}

	public async login() {
		const email = this.r["email"].value;

		await sleep();

		this.httpClient
			.post(`${environment.API_URL}/login`, { email, uuid: uuid() })
			.subscribe((response: any) => {
				const message: string = response.message;

				if (message === "User not exist") {
					this.toastr.warning("Cet utilisateur n'existe pas.");
				} else if (message === "User logged in") {
					const user: IUser = response.user;
					const token: string = response.token;
					this.userService.user = { ...user, token };

					setToStorage("token", token);
					this.handleConnected.emit();
					this.registrationLoginFormGroup.reset();
				}

				this.loaderRegistrationLogin = false;
			});
	}
}
