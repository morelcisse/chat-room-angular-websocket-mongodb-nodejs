export interface IUser {
	_id: string;
	name: string;
	email: string;
	role: string;
	imagePath: string;
	connected?: boolean;
	token?: string;
}

export interface IMessage {
	_id: string;
	channel_name: string;
	message: string;
	user: IUser;
}
