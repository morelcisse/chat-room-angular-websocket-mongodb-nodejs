# CHATROOM

### PREREQUISITES

- **NodeJS**: *18.12.1*
- **NPM**: *8.19.2*

### STACK

Angular v15 (Typescript) , Websocket, Node, Mongodb cloud, SCSS [...]

### Database

You can create a account on mongodb cloud, it's free !
> https://cloud.mongodb.com/

### Setup

- Create **chat** database
- Create **users** and **messages** collections
- Copy .env.example to .env (<[rootDir]>/server)
- Update **MONGODB_URI / MONGODB_APPNAME** vars in .env file (<[rootDir]>/server/.env)

#### Install

```sh
npm i --legacy-peer-deps
```

#### Start server

```sh
npm run server:start
```

#### Start client

```sh
npm run start
```

#### Instructions

- Open http://localhost:4200 in 2 or more browsers
- Create or use your different logins to enter the room
- Interact !

---

[PREVIEW LINK](https://gitlab.com/morelcisse/chat-room-angular-websocket-mongodb-nodejs/-/blob/master/preview.mp4)
