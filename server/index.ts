import express from "express";
import path from "path";
import cors from "cors";
import { json } from "body-parser";

require("dotenv").config({ path: path.resolve(__dirname, ".env") });

const dev = process.env["NODE_ENV"] === "development";

const { connect, close } = require("./helpers/mongodb");

const app = express();

const PORT = process.env["PORT"] || 3000;

app.use(cors());
app.use(json());

if (!dev) app.use(express.static(path.join(`${__dirname}/../`)));

// Routes
app.use(require("./helpers/routes"));

const loadSocketIo = (server) => {
	const io = require("socket.io")(server, {
		cors: {
			origin: ["http://localhost:4200"],
			credentials: false,
		},
		cookie: {
			name: "io",
			httpOnly: true,
			path: "/",
		},
	}); // Socket Layer over Http Server

	io.on("connection", (client) => {
		// On every Client Connection
		console.log("Socket: client connected", client.id);

		client.on("join_room", (data) => {
			const channelName = data.channelName;
			const user = data.user;

			client.join(channelName);
			client.broadcast.emit("join_room_notify", { channelName, user });
		});

		client.on("leave_room", (data) => {
			const channelName = data.channelName;
			const user = data.user;

			client.leave(channelName); // We are using room of client io
			client.broadcast.emit("leave_room_notify", { channelName, user });
			client.removeAllListeners();
		});

		client.on("new_registration", (data) => {
			const user = data.user;
			const channelName = data.channelName;

			client.broadcast.emit("new_registration_notify", { channelName, user });
		});

		client.on("new_message", (data) => {
			const channelName = data.channelName;
			const message = data.message;

			client.broadcast.emit(channelName, { message });
		});

		client.on("disconnect", () => {
			console.log("Disconnected client", client.id);

			client.removeAllListeners();
		});
	});
};

connect(() => {
	/* -------------------------------------------------------------------------- */
	/*                                WITH MONGODB                                */
	/* -------------------------------------------------------------------------- */
	const server = app.listen(PORT, () => console.log(`Server listen on PORT ${PORT}`));

	loadSocketIo(server);
});

module.exports = app;
