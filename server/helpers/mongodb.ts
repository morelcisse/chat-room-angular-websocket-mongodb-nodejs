import { Db } from "mongodb";
import { MongoClient } from "mongodb";

let db: Db;
let client: MongoClient;

const connect = (cb: Function) => {
	MongoClient.connect(process.env["MONGODB_URI"] as string, {
		appName: process.env["MONGODB_APPNAME"],
	})
		.then((res) => {
			client = res as MongoClient;
			db = client.db("chat");

			cb();
		})
		.catch((err) => {
			console.log("error", err);
		});
};

const getDB = () => db;
const close = () => client.close();

export { connect, getDB, close };
