import { NextFunction, Request, Response } from "express";

const jwt = require("jsonwebtoken"); // https://www.npmjs.com/package/jsonwebtoken

const authenticateToken = (req: Request, res: Response, next: NextFunction): any => {
	// Gather the jwt access token from the request header
	const authHeader = req.headers["authorization"];
	const token = authHeader && authHeader.split(" ")[1];
	// if (token == null) return res.sendStatus(401); // if there isn't any token
	if (token == null) return res.json({ message: "Token not found" });

	jwt.verify(token, process.env["JWT_TOKEN_SECRET"], (err: any) => {
		console.log("authenticateToken err", err);

		if (err) {
			if (err.name === "TokenExpiredError") return res.json({ message: "Token expired" });
			if (err.name === "JsonWebTokenError") return res.json({ message: "Token invalid" });
		}

		next(); // pass the execution off to whatever request the client intended
	});
};

const generateAccessToken = (email: string) => {
	// expires after half and hour (1800 seconds = 30 minutes)
	return jwt.sign({ email }, process.env["JWT_TOKEN_SECRET"], { expiresIn: "2h" });
};

export { authenticateToken, generateAccessToken };
