import express from "express";
import jwt from "jsonwebtoken"; // https://www.npmjs.com/package/jsonwebtoken

import { getDB } from "./mongodb";
import { authenticateToken, generateAccessToken } from "./jwt";
import { capitalize } from "./functions";

const Router = express.Router();

/* -------------------------------------------------------------------------- */
/*                                    USERS                                   */
/* -------------------------------------------------------------------------- */

Router.post("/verify", (req, res) => {
	const token = req.body.token;

	jwt.verify(token, process.env["JWT_TOKEN_SECRET"] as string, async (err: any, { email }: any) => {
		if (err) {
			const message = err.name === "TokenExpiredError" ? "Token expired" : "Token invalid";

			res.json({ message });
			return;
		}

		const user = await getDB().collection("users").findOne({ email });

		if (user) {
			let message = user ? "User found" : "User not exist";
			// rome-ignore lint/complexity/useSimplifiedLogicExpression: <explanation>
			const connected = user?.["connected"] || false;

			if (!connected) message = "User not connected";

			res.json({ message, user });
		}
	});
});

/* -------------------------------------------------------------------------- */
/*                                  MESSAGES                                  */
/* -------------------------------------------------------------------------- */

Router.post("/create/message", authenticateToken, async (req, res) => {
	const body = req.body;
	const channelName = body.channelName;
	const user = body.user;
	let data = {
		user,
		message: body.message,
		channel_name: channelName,
	};
	const message = await getDB().collection("messages").findOne({ message: req.body.message });

	if (message) return res.json({ message });

	await getDB().collection("messages").insertOne(data);

	res.json({ message: data });
});

Router.get("/messages", authenticateToken, async (req, res) => {
	const messages = await getDB().collection("messages").find({}).toArray();

	res.json(messages);
});

/* -------------------------------------------------------------------------- */
/*                                 CONNECTION                                 */
/* -------------------------------------------------------------------------- */

Router.post("/registration", async (req, res) => {
	const email = req.body.email;
	const uuid = req.body.uuid;
	const name = capitalize(req.body.name);
	let message = "User created";
	const user = await getDB().collection("users").findOne({ email });
	const data = {
		email,
		name,
		role: "guest",
		imagePath: "/assets/avatar.svg",
		connected: true,
		uuid,
	};

	if (user) {
		message = "User already exist";

		return res.json({ message });
	} else {
		await getDB().collection("users").insertOne(data);

		const user = await getDB().collection("users").findOne({ email });
		const token = generateAccessToken(email);

		res.json({ message, user, token });
	}
});

Router.post("/login", async (req, res) => {
	const email = req.body.email;
	const uuid = req.body.uuid;
	let message = "User not exist";
	const user = await getDB().collection("users").findOne({ email });

	if (user) {
		const token = generateAccessToken(email);
		message = "User logged in";

		await getDB()
			.collection("users")
			.updateOne({ email }, { $set: { connected: true, uuid } });

		const newuser = { ...user, connected: true };

		return res.json({ message, user: newuser, token });
	}

	res.json({ message });
});

Router.post("/logout", async (req, res) => {
	const message = "User logged out";
	const userId = req.body.userId;
	const ObjectId = require("mongodb").ObjectId;

	await getDB()
		.collection("users")
		.updateOne({ _id: new ObjectId(`${userId}`) }, { $set: { connected: false } });

	res.json({ message });
});

module.exports = Router;
